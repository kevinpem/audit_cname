% CNAM - Audit Amélie-Réseau - 31/01/2023
% Atos Open Source
%

Présentation déroulement Audit

# Contexte

Audit de performance sur l'application Amélie-Réseau

# Prérequis

## Modules Drupal
- Site Audit
- Unused modules
- Devel
- script maison (modules actif / passif)

## Apache

### apache2buddy
- Cet outil fournit une préconisation des paramètres apache suivant :
  - PHP memory limits
  - Memcache
  - MaxClients
  - Memory usage
  - Max potential memory usage
  - Percentage of total RAM allocated to Apache
  - MaxRequestWorkers

## Serveur de base de données

### mysql_tuner
- https://github.com/major/MySQLTuner-perl

### memcached-top 
- https://github.com/eculver/memcache-top


# F5

N/A

# Serveur (apache / httpd) - PHP - Drupal

## OS
- Ram disponible
- CPU

## Apache
- Version httpd

## PHP
- Version php

## Serveur Drupal
- Version drupal
- Module audit
- Performance
- Requêtes lentes

# Serveur de base de données (MySQL - Memcached)

## OS
- Ram disponible
- CPU

## Memcached
- memcache-top
- Verifier les paramètres utilisés : /etc/sysconfig/memcached

## MySQL
- Executer mysql_tuner
- Status global
- Status variable

# Monitoring
- Nombre de connexions sur https://ameli-reseau.ramage
  - Par heures
- Consommation du CPU pour chaque serveur
- Consommation de la mémoire (RAM) pour chaque serveur
- Consommation du réseau
- Consommation des serveurs Apache
- Consommation des serveurs Drupal
- Consommation des serveurs Memcache
- Consommation de la base de données MySQL pour chaque serveur

# Action / Préconisation

## Contournement
- Apache
- Drupal/memcached
- Memcached

## Préconisation
- Drupal
- MySQL